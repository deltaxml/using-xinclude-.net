# Using XInclude

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/using-xinclude`.*

---

An example pipeline that instructs the XML parser to process XInclude statements before passing the input data to XML Compare.

This document describes how to run the sample. For concept details see: [XInclude and XML Compare](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/xinclude-and-xml-compare)

## Running the Sample

The sample can be run via a *run.bat* batch file, so long as this is issued from the sample directory. Alternatively, the commands can be executed directly:

	..\..\bin\deltaxml.exe compare xinclude-demo inputs/input1-full.xml inputs/input2-includes.xml non-include-result.xml enable-xinclude=false
	..\..\bin\deltaxml.exe compare xinclude-demo inputs/input1-full.xml inputs/input2-includes.xml include-result.xml enable-xinclude=true
	
# **Note - .NET support has been deprecated as of version 10.0.0 **